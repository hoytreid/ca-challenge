USE [master]
GO
/****** Object:  Database [StoreData]    Script Date: 08/02/2019 14:34:42 ******/
CREATE DATABASE [StoreData]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'StoreData', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\StoreData.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'StoreData_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\StoreData_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [StoreData] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StoreData].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StoreData] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StoreData] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StoreData] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StoreData] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StoreData] SET ARITHABORT OFF 
GO
ALTER DATABASE [StoreData] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [StoreData] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StoreData] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StoreData] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StoreData] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StoreData] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StoreData] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StoreData] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StoreData] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StoreData] SET  ENABLE_BROKER 
GO
ALTER DATABASE [StoreData] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StoreData] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StoreData] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StoreData] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StoreData] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StoreData] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [StoreData] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StoreData] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [StoreData] SET  MULTI_USER 
GO
ALTER DATABASE [StoreData] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StoreData] SET DB_CHAINING OFF 
GO
ALTER DATABASE [StoreData] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [StoreData] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [StoreData] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [StoreData] SET QUERY_STORE = OFF
GO
USE [StoreData]
GO
/****** Object:  Table [dbo].[SalesData]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesData](
	[StoreID] [int] NOT NULL,
	[TotalSalesWeeks13] [money] NOT NULL,
	[TotalVolumeWeeks13] [int] NOT NULL,
	[AverageProfitWeeks13] [money] NOT NULL,
	[WeekCounter13] [int] NOT NULL,
	[TotalSalesWeeks52] [money] NOT NULL,
	[TotalVolumeWeeks52] [int] NOT NULL,
	[AverageProfitWeeks52] [money] NOT NULL,
	[WeekCounter52] [int] NOT NULL,
	[Bay] [int] NOT NULL,
	[FloorName] [nvarchar](50) NOT NULL,
	[ClusterGroupID] [int] NOT NULL,
	[ClusterID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[pr_GetAverageProfitByStore]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetAverageProfitByStore] @VarStoreId int
	AS
	BEGIN
		SELECT DISTINCT AverageProfitWeeks13,AverageProfitWeeks52, Bay, FloorName
			FROM dbo.SalesData
			WHERE StoreID = @VarStoreId
			AND WeekCounter13 = 13 AND WeekCounter52 = 52
			ORDER BY Bay
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetAverageProfitPerBay]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetAverageProfitPerBay] @VarStoreId int, @VarFloorName varchar(50)
	AS
	BEGIN
		SELECT DISTINCT AverageProfitWeeks13, AverageProfitWeeks52, Bay
			FROM dbo.SalesData
			WHERE StoreID = @VarStoreId AND FloorName = @VarFloorName
			AND WeekCounter13 = 13 AND WeekCounter52 = 52
			ORDER BY Bay
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetClusterIds]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetClusterIds] @VarClusterGroupId int
	AS
	BEGIN
		SELECT DISTINCT ClusterID
		FROM dbo.SalesData
		WHERE ClusterGroupID = @VarClusterGroupId
		ORDER BY ClusterID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetStoreIds]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetStoreIds]
	AS
	BEGIN
		SELECT DISTINCT StoreID FROM dbo.SalesData
			ORDER BY StoreID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetTotalSalesByStore]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetTotalSalesByStore] @VarStoreId int
	AS
	BEGIN
		SELECT DISTINCT TotalSalesWeeks13, TotalSalesWeeks52, Bay, FloorName
			FROM dbo.SalesData
			WHERE StoreID = @VarStoreId
			AND WeekCounter13 = 13 AND WeekCounter52 = 52
			ORDER BY Bay
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetTotalSalesPerBay]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetTotalSalesPerBay] @VarStoreId int, @VarFloorName varchar(50)
	AS
	BEGIN
		SELECT DISTINCT TotalSalesWeeks13, TotalSalesWeeks52, Bay
			FROM dbo.SalesData
			WHERE StoreID = @VarStoreId AND FloorName = @VarFloorName
			AND WeekCounter13 = 13 AND WeekCounter52 = 52
			ORDER BY Bay
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetTotalVolumeByStore]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetTotalVolumeByStore] @VarStoreId int
	AS
	BEGIN
		SELECT DISTINCT TotalVolumeWeeks13, TotalVolumeWeeks52, Bay, FloorName
			FROM dbo.SalesData
			WHERE StoreID = @VarStoreId
			AND WeekCounter13 = 13 AND WeekCounter52 = 52
			ORDER BY Bay
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetTotalVolumePerBay]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetTotalVolumePerBay] @VarStoreId int, @VarFloorName varchar(50)
	AS
	BEGIN
		SELECT DISTINCT TotalVolumeWeeks13, TotalVolumeWeeks52, Bay
			FROM dbo.SalesData
			WHERE StoreID = @VarStoreId AND FloorName = @VarFloorName
			AND WeekCounter13 = 13 AND WeekCounter52 = 52
			ORDER BY Bay
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetWeek13ProfitByCluster]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetWeek13ProfitByCluster] @VarClusterId int
	AS
	BEGIN
		SELECT StoreID, SUM(AverageProfitWeeks13) AS AverageProfitWeeks13
			FROM dbo.SalesData
			WHERE ClusterId = @VarClusterId
				AND WeekCounter13 = 13
			GROUP BY StoreID
			ORDER BY StoreID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetWeek13SalesByCluster]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetWeek13SalesByCluster] @VarClusterId int
	AS
	BEGIN
		SELECT StoreID, SUM(TotalSalesWeeks13) AS TotalSalesWeeks13
			FROM dbo.SalesData
			WHERE ClusterId = @VarClusterId
				AND WeekCounter13 = 13
			GROUP BY StoreID
			ORDER BY StoreID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetWeek13VolumeByCluster]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetWeek13VolumeByCluster] @VarClusterId int
	AS
	BEGIN
		SELECT StoreID, SUM(TotalVolumeWeeks13) AS TotalVolumeWeeks13
			FROM dbo.SalesData
			WHERE ClusterId = @VarClusterId
				AND WeekCounter13 = 13
			GROUP BY StoreID
			ORDER BY StoreID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetWeek52ProfitByCluster]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetWeek52ProfitByCluster] @VarClusterId int
	AS
	BEGIN
		SELECT StoreID, SUM(AverageProfitWeeks52) AS AverageProfitWeeks52
			FROM dbo.SalesData
			WHERE ClusterId = @VarClusterId
				AND WeekCounter52 = 52
			GROUP BY StoreID
			ORDER BY StoreID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetWeek52SalesByCluster]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetWeek52SalesByCluster] @VarClusterId int
	AS
	BEGIN
		SELECT StoreID, SUM(TotalSalesWeeks52) AS TotalSalesWeeks52
			FROM dbo.SalesData
			WHERE ClusterId = @VarClusterId
				AND WeekCounter52 = 52
			GROUP BY StoreID
			ORDER BY StoreID;
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_GetWeek52VolumeByCluster]    Script Date: 08/02/2019 14:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_GetWeek52VolumeByCluster] @VarClusterId int
	AS
	BEGIN
		SELECT StoreID, SUM(TotalVolumeWeeks52) AS TotalVolumeWeeks52
			FROM dbo.SalesData
			WHERE ClusterId = @VarClusterId
				AND WeekCounter52 = 52
			GROUP BY StoreID
			ORDER BY StoreID;
	END
GO
USE [master]
GO
ALTER DATABASE [StoreData] SET  READ_WRITE 
GO
