﻿using CA_Challenge.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;

namespace CA_Challenge.Controllers
{
    public class ChartController : Controller
    {
        public FileResult RenderStoreSalesChart(int storeId, string floorName)
        {
            SqlParamHelper[] parameters = new SqlParamHelper[]
            {
                new SqlParamHelper("@VarStoreId", SqlDbType.Int, storeId),
                new SqlParamHelper("@VarFloorName", SqlDbType.NVarChar, floorName)
            };

            StoredProcedureController salesProcedure = new StoredProcedureController("pr_GetTotalSalesPerBay");
            DataTable records;

            records = salesProcedure.Execute(parameters);

            Chart chart = new Chart
            {
                Width = 1500,
                Height = 750
            };

            chart.Series.Add(CreateSeries(records, "TotalSalesWeeks13", "Week 13", Color.FromArgb(188, 188, 203)));
            chart.Series.Add(CreateSeries(records, "TotalSalesWeeks52", "Week 52", Color.FromArgb(86, 217, 254)));
            chart.Legends.Add(CreateLegend());
            chart.ChartAreas.Add(CreateChartArea("Bay Number", "Sales Amount (£)"));

            var returnVal = new MemoryStream();
            chart.SaveImage(returnVal);
            return File(returnVal.GetBuffer(), @"image/png");
        }

        public FileResult RenderStoreVolumeChart(int storeId, string floorName)
        {
            SqlParamHelper[] parameters = new SqlParamHelper[]
            {
                new SqlParamHelper("@VarStoreId", SqlDbType.Int, storeId),
                new SqlParamHelper("@VarFloorName", SqlDbType.NVarChar, floorName)
            };

            StoredProcedureController volumeProcedure = new StoredProcedureController("pr_GetTotalVolumePerBay");
            DataTable records;

            records = volumeProcedure.Execute(parameters);

            Chart chart = new Chart
            {
                Width = 1500,
                Height = 750
            };

            chart.Series.Add(CreateSeries(records, "TotalVolumeWeeks13", "Week 13", Color.FromArgb(188, 188, 203)));
            chart.Series.Add(CreateSeries(records, "TotalVolumeWeeks52", "Week 52", Color.FromArgb(163, 161, 251)));
            chart.Legends.Add(CreateLegend());
            chart.ChartAreas.Add(CreateChartArea("Bay Number", "Volume"));

            var returnVal = new MemoryStream();
            chart.SaveImage(returnVal);
            return File(returnVal.GetBuffer(), @"image/png");
        }

        public FileResult RenderStoreProfitChart(int storeId, string floorName)
        {
            SqlParamHelper[] parameters = new SqlParamHelper[]
            {
                new SqlParamHelper("@VarStoreId", SqlDbType.Int, storeId),
                new SqlParamHelper("@VarFloorName", SqlDbType.NVarChar, floorName)
            };

            StoredProcedureController volumeProcedure = new StoredProcedureController("pr_GetAverageProfitPerBay");
            DataTable records;

            records = volumeProcedure.Execute(parameters);

            Chart chart = new Chart
            {
                Width = 1500,
                Height = 750
            };

            chart.Series.Add(CreateSeries(records, "AverageProfitWeeks13", "Week 13", Color.FromArgb(188, 188, 203)));
            chart.Series.Add(CreateSeries(records, "AverageProfitWeeks52", "Week 52", Color.FromArgb(255, 131, 115)));
            chart.Legends.Add(CreateLegend());
            chart.ChartAreas.Add(CreateChartArea("Bay Number", "Average Profit (£)"));

            var returnVal = new MemoryStream();
            chart.SaveImage(returnVal);
            return File(returnVal.GetBuffer(), @"image/png");
        }

        public FileResult RenderClusterChart(int clusterId, string procedure)
        {
            Chart chart = new Chart
            {
                Width = 750,
                Height = 750
            };

            List<double> yValues = new List<double>();
            List<string> xValues = new List<string>();

            SqlParamHelper[] parameters = new SqlParamHelper[]
            {
                new SqlParamHelper("@VarClusterId", SqlDbType.Int, clusterId)
            };

            StoredProcedureController clusterProcedure = new StoredProcedureController(procedure);
            DataTable records;

            records = clusterProcedure.Execute(parameters);

            foreach (DataRow row in records.Rows)
            {
                yValues.Add(Convert.ToDouble(row[1]));
                xValues.Add("Store " + row[0].ToString());
            }

            string titleText = "";
            if (procedure.Contains("Week52"))
            {
                titleText = "Week 52";
            }
            else if (procedure.Contains("Week13"))
            {
                titleText = "Week 13";
            }
            else
            {
                titleText = "Week ??";
            }

            int[] rgb = GetChartColor(procedure);

            chart.Series.Add(CreatePieSeries(yValues, xValues, rgb));
            chart.Legends.Add(new Legend());
            chart.ChartAreas.Add(new ChartArea());
            chart.Titles.Add(CreateTitle(titleText));

            var returnVal = new MemoryStream();
            chart.SaveImage(returnVal);
            return File(returnVal.GetBuffer(), @"image/png");
        }

        private static int[] GetChartColor(string procedure)
        {
            int[] rgb;
            if (procedure.Contains("Sales"))
            {
                rgb = new int[] { 86, 217, 254 };
            }
            else if (procedure.Contains("Volume"))
            {
                rgb = new int[] { 163, 161, 251 };
            }
            else if (procedure.Contains("Profit"))
            {
                rgb = new int[] { 255, 131, 115 };
            }
            else
            {
                rgb = new int[] { 188, 188, 203 };
            }

            return rgb;
        }

        private static Title CreateTitle(string text)
        {
            Title title = new Title
            {
                Text = text,
                ForeColor = Color.FromArgb(127, 67, 66, 90),
                Font = new Font("Trebuchet MS", 14F, FontStyle.Bold),
                Alignment = ContentAlignment.TopLeft
            };
            return title;
        }

        private static Series CreatePieSeries(List<double> yValues, List<string> xValues, int[] rgb)
        {
            Series seriesDetail = new Series
            {
                ChartType = SeriesChartType.Pie
            };
            seriesDetail.Points.DataBindXY(xValues, yValues);
            seriesDetail.Label = "#PERCENT{P0}";

            seriesDetail["PieLabelStyle"] = "Outside";
            seriesDetail["PieLineColor"] = "Black";

            for (int i = 0; i < seriesDetail.Points.Count; i++)
            {
                int r = (rgb[0] + (20 * i)) > 255 ? 255 : rgb[0] + (20 * i);
                int g = (rgb[1] + (20 * i)) > 255 ? 255 : rgb[1] + (20 * i);
                int b = (rgb[2] + (20 * i)) > 255 ? 255 : rgb[2] + (20 * i);

                seriesDetail.Points[i].LegendText = "#VALX";
                seriesDetail.Points[i].Color = Color.FromArgb(r, g, b);
            }

            return seriesDetail;
        }

        public Series CreateSeries(DataTable records, string rowName, string label, Color color)
        {
            Series seriesDetail = new Series
            {
                ChartType = SeriesChartType.Column,
                Color = color,
                LegendText = label
            };

            foreach (DataRow row in records.Rows)
            {
                DataPoint point = new DataPoint
                {
                    AxisLabel = row["Bay"].ToString(),
                    YValues = new double[] { Convert.ToDouble(row[rowName]) }
                };
                seriesDetail.Points.Add(point);
            }

            return seriesDetail;
        }

        public Legend CreateLegend()
        {
            Legend legend = new Legend
            {
                Docking = Docking.Top,
                Alignment = StringAlignment.Near
            };

            return legend;
        }

        public ChartArea CreateChartArea(string xAxisLabel, string yAxisLabel)
        {
            ChartArea chartArea = new ChartArea();

            chartArea.AxisX.Title = xAxisLabel;
            chartArea.AxisY.Title = yAxisLabel;

            chartArea.AxisX.LineColor = Color.FromArgb(127, 67, 66, 90);
            chartArea.AxisX.LabelStyle.ForeColor = Color.FromArgb(127, 67, 66, 90);
            chartArea.AxisY.LineColor = Color.FromArgb(127, 67, 66, 90);
            chartArea.AxisY.LabelStyle.ForeColor = Color.FromArgb(127, 67, 66, 90);
            chartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(234, 240, 244);
            chartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(234, 240, 244);

            return chartArea;
        }
    }
}