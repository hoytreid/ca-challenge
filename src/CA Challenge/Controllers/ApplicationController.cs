﻿using CA_Challenge.Data;
using CA_Challenge.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using static CA_Challenge.Data.Clusters;

namespace CA_Challenge.Controllers
{
    public abstract class ApplicationController : Controller
    {
        public ApplicationController()
        {
            StoredProcedureController stores = new StoredProcedureController("pr_GetStoreIds");
            ViewBag.StoreIds = stores.Execute("StoreId");

            StoredProcedureController regions = new StoredProcedureController("pr_GetClusterIds");
            List<Cluster> regionList = regions.ExecuteCluster(ClusterGroup.Region);
            ViewBag.Regions = regionList;

            StoredProcedureController locations = new StoredProcedureController("pr_GetClusterIds");
            List<Cluster> locationList = locations.ExecuteCluster(ClusterGroup.Location);
            ViewBag.Locations = locationList;

            StoredProcedureController sizes = new StoredProcedureController("pr_GetClusterIds");
            List<Cluster> sizeList = sizes.ExecuteCluster(ClusterGroup.Size);
            ViewBag.Sizes = sizeList;
        }
    }
}