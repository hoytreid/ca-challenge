﻿using CA_Challenge.Data;
using System;
using System.Data;
using System.Web.Mvc;

namespace CA_Challenge.Controllers
{
    public class HomeController : ApplicationController
    {
        public ActionResult Index()
        { 
            return View();
        }

        public ActionResult Store(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SqlParamHelper idParam = new SqlParamHelper("@VarStoreId", SqlDbType.Int, id);

            StoredProcedureController storeSales = new StoredProcedureController("pr_GetTotalSalesByStore");
            DataTable salesTable = storeSales.Execute(new SqlParamHelper[] { idParam });
            decimal salesSum = Convert.ToDecimal(salesTable.Compute("Sum(TotalSalesWeeks52)", string.Empty));
            ViewBag.TotalSales = string.Format("{0:n}", decimal.Round(salesSum, 2, MidpointRounding.AwayFromZero));

            StoredProcedureController storeVolume = new StoredProcedureController("pr_GetTotalVolumeByStore");
            DataTable volumeTable = storeVolume.Execute(new SqlParamHelper[] { idParam });
            ViewBag.TotalVolume = string.Format("{0:n0}", volumeTable.Compute("Sum(TotalVolumeWeeks52)", string.Empty));

            StoredProcedureController storeProfit = new StoredProcedureController("pr_GetAverageProfitByStore");
            DataTable profitTable = storeProfit.Execute(new SqlParamHelper[] { idParam });
            decimal profitSum = Convert.ToDecimal(profitTable.Compute("Sum(AverageProfitWeeks52)", string.Empty));
            ViewBag.AverageProfit = string.Format("{0:n}", decimal.Round(profitSum, 2, MidpointRounding.AwayFromZero));

            ViewBag.StoreId = id;

            return View();
        }

        public ActionResult Region(int id)
        {
            ViewBag.Title = Clusters.GetClusterName(id);
            ViewBag.RegionId = id;
            return View();
        }

        public ActionResult Location(int id)
        {
            ViewBag.Title = Clusters.GetClusterName(id);
            ViewBag.LocationId = id;
            return View();
        }

        public ActionResult Size(int id)
        {
            ViewBag.Title = Clusters.GetClusterName(id);
            ViewBag.SizeId = id;
            return View();
        }
    }
}