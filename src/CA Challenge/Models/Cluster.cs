﻿namespace CA_Challenge.Models
{
    public class Cluster
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Cluster(int clusterId, string clusterName)
        {
            Id = clusterId;
            Name = clusterName;
        }
    }
}