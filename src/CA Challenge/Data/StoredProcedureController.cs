﻿using CA_Challenge.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using static CA_Challenge.Data.Clusters;

namespace CA_Challenge.Data
{
    public class StoredProcedureController
    {
        public string Procedure { get; set; }

        public StoredProcedureController(string storedProcedure)
        {
            Procedure = storedProcedure;
        }

        public List<object> Execute(string columnName)
        {
            List<object> values = new List<object>();
            using(SqlConnection connection = new SqlConnection(DbConnect.ConnectionString))
            {
                SqlCommand command = new SqlCommand(Procedure, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    values.Add(reader[columnName]);
                }
            }
            return values;
        }

        public DataTable Execute(params SqlParamHelper[] parameters)
        {
            DataTable data = new DataTable();
            using (SqlConnection connection = new SqlConnection(DbConnect.ConnectionString))
            {
                SqlCommand command = new SqlCommand(Procedure, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                for (int i = 0; i < parameters.Length; i++)
                {
                    var parameter = parameters[i];
                    command.Parameters.Add(parameter.Name, parameter.DbType).Value = parameter.Value;
                }
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(data);
            }
            return data;
        }

        public List<Cluster> ExecuteCluster(ClusterGroup clusterGroupId)
        {
            List<Cluster> clusters = new List<Cluster>();
            using (SqlConnection connection = new SqlConnection(DbConnect.ConnectionString))
            {
                SqlCommand command = new SqlCommand(Procedure, connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add("@VarClusterGroupId", SqlDbType.Int).Value = clusterGroupId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int clusterId = (int)reader["ClusterId"];
                    string clusterString;
                    ClusterNames.TryGetValue(clusterId, out clusterString);
                    clusters.Add(new Cluster(clusterId, clusterString));
                }
            }

            return clusters;
        }
    }
}