﻿using System.Configuration;
using System.Data.SqlClient;

namespace CA_Challenge.Data
{
    public static class DbConnect
    {
        public static string ConnectionString { get; } = ConfigurationManager.ConnectionStrings["MCSConnection"].ConnectionString;
        public static SqlConnection Connection { get; } = new SqlConnection(ConnectionString);

        public static void Open()
        {
            Connection.Open();
        }

        public static void Close()
        {
            Connection.Close();
        }
    }
}