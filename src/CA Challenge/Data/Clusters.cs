﻿using System.Collections.Generic;

namespace CA_Challenge.Data
{
    public static class Clusters
    {
        public enum ClusterGroup
        {
            Region = 1,
            Location = 3,
            Size = 4
        }

        public static Dictionary<int, string> ClusterNames = new Dictionary<int, string>()
        {
            { 2, "North" },
            { 3, "South" },
            { 4, "East" },
            { 5, "West" },

            { 8, "High Street" },
            { 9, "Retail Park" },
            { 10, "Shoping Center" },

            { 11, "< 25,000" },
            { 12, "25,000 - 50,000" },
            { 13, "50,000 - 75,000" },
            { 14, "75,000 +" }
        };

        public static string GetClusterName(int id)
        {
            ClusterNames.TryGetValue(id, out string name);
            return name;
        }
    }
}