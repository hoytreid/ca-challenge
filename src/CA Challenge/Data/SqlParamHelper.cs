﻿using System.Data;

namespace CA_Challenge.Data
{
    public class SqlParamHelper
    {
        public string Name { get; }
        public SqlDbType DbType { get; }
        public object Value { get; }

        public SqlParamHelper(string name, SqlDbType dbType, object value)
        {
            this.Name = name;
            this.DbType = dbType;
            this.Value = value;
        }
    }
}